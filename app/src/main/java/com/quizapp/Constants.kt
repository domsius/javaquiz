package com.quizapp



object Constants {

    // TODO (STEP 1: Create a constant variables which we required in the result screen.)
    // START
    const val USER_NAME: String = "user_name"
    const val TOTAL_QUESTIONS: String = "total_questions"
    const val CORRECT_ANSWERS: String = "correct_answers"
    // END
    
    fun getQuestions(): ArrayList<Question> {
        val questionsList = ArrayList<Question>()

        // 1
        val que1 = Question(
            1, "Which of the following is not a keyword in java??",
            R.drawable.java,
            "static", "boolean",
            "void", "private", 2
        )

        questionsList.add(que1)

        // 2
        val que2 = Question(
            2, "What is the size of double variable?",
            R.drawable.java,
            "8 bit", "16 bit",
            "32 bit", "64 bit", 4
        )

        questionsList.add(que2)

        // 3
        val que3 = Question(
            3, "What is the default value of long variable?",
            R.drawable.java,
            "0", "0.0",
            "0L", "not defined", 3
        )

        questionsList.add(que3)

        // 4
        val que4 = Question(
            4, "Which of the following is true about String?",
            R.drawable.java,
            "String is mutable.", "B - String is immutable.",
            "String is a data type.", "D - None of the above.", 2
        )

        questionsList.add(que4)

        // 5
        val que5 = Question(
            5, "What is an Interface?",
            R.drawable.java,
            "An interface is a collection of abstract methods.", "Interface is an abstract class.",
            "Interface is an concrete class.", "None of the above.", 3
        )

        questionsList.add(que5)

        // 6
        val que6 = Question(
            6, "What kind of variables a class can consist of?",
            R.drawable.java,
            "class variables, instance variables", "class variables, local variables, instance variables",
            "class variables", "class variables, local variables", 2
        )

        questionsList.add(que6)

        // 7
        val que7 = Question(
            7, "Static binding uses which information for binding?",
            R.drawable.java,
            "type.", "object.",
            "Both of the above.", "None of the above.", 1
        )

        questionsList.add(que7)

        // 8
        val que8 = Question(
            8, "What is a transient variable??",
            R.drawable.java,
            "A transient variable is a variable which is serialized during Serialization.", "A transient variable is a variable that may not be serialized during Serialization.",
            "A transient variable is a variable which is to be marked as serializable.", "None of the above.", 4
        )

        questionsList.add(que8)

        // 9
        val que9 = Question(
            9, "Can constructor be inherited?",
            R.drawable.java,
            "True", "False",
            "None of the above", "Both of the above", 2
        )

        questionsList.add(que9)

        // 10
        val que10 = Question(
            10, "Which of the following is a thread safe?",
            R.drawable.java,
            "StringBuilder", "StringBuffer",
            "Both of the above", "None of the above", 2
        )

        questionsList.add(que10)

        return questionsList
    }
}